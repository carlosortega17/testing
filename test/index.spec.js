const request = require('supertest');
const app = require('../app');


describe('Calculadora', () => {
  it('GET /', async () => {
    const res = await request(app).get('/');
    expect(res).toBeDefined();
    expect(res.status).toEqual(200);
    expect(JSON.parse(res.text)).toEqual({
      title: 'Status',
      message: 'OK',
    });
  });
});